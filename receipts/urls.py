from django.urls import path
from receipts.views import (
    show_Receipt,
    create_receipt,
    category_list,
    account_list,
    create_Category,
    create_account,
)

urlpatterns = [
    path("", show_Receipt, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="accounts_list"),
    path("categories/create/", create_Category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
